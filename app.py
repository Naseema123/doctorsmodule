from flask import Flask, render_template, request, flash, redirect, url_for
from datetime import datetime
import random

app = Flask(__name__)
app.secret_key = 'your_secret_key_here'  # Change this to a secure secret key

# Sample patient data
patients = {}

class Patient:
    def __init__(self, name, age, mobile_no, location, id):
        self.name = name
        self.age = age
        self.mobile_no = mobile_no
        self.location = location
        self.id = id
        self.clinical = []

    def add_clinical_data(self, test_name, test_value):
        timestamp = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        self.clinical.append({"Test Name": test_name, "Test Value": test_value, "Timestamp": timestamp})

    def display_clinical_data(self):
        if self.clinical:
            return f"Patient ID: {self.id}<br>Patient Name: {self.name}<br>Patient Age: {self.age}<br>Mobile No: {self.mobile_no}<br>Location: {self.location}<br>Clinical Data:<br>" + \
                "<br>".join([f"Test Name: {data['Test Name']}<br>Test Value: {data['Test Value']}<br>Timestamp: {data['Timestamp']}" for data in self.clinical])
        else:
            return f"Patient ID: {self.id}<br>Patient Name: {self.name}<br>Patient Age: {self.age}<br>Mobile No: {self.mobile_no}<br>Location: {self.location}<br>No clinical data available for this patient."

# Routes
@app.route('/')
def index():
    return render_template('index.html')

@app.route('/add_patient', methods=['POST'])
def add_patient():
    name = request.form['name']
    age = request.form['age']
    mobile_no = request.form['mobile_no']
    location = request.form['location']
    
    if name and age and mobile_no and location:
        if mobile_no not in patients:
            id = generate_patient_id()
            patients[mobile_no] = Patient(name, age, mobile_no, location, id)
            flash(f"Patient added successfully. ID: {id}", 'success')
        else:
            flash("Mobile number already exists for another patient.", 'error')
    else:
        flash("All fields (Name, Age, Mobile No, Location) are required.", 'error')
    
    return redirect(url_for('index'))

@app.route('/add_clinical_data', methods=['POST'])
def add_clinical_data():
    search_criteria = request.form['search_criteria']
    test_name = request.form['test_name']
    test_value = request.form['test_value']
    
    found_patient = None
    for patient in patients.values():
        if (
            search_criteria.lower() in patient.name.lower() or
            search_criteria == patient.id or
            search_criteria == patient.mobile_no
        ):
            found_patient = patient
            break
    
    if found_patient:
        if test_name and test_value:
            found_patient.add_clinical_data(test_name, test_value)
            flash("Clinical data added successfully.", 'success')
        else:
            flash("Test name and test value are required fields.", 'error')
    else:
        flash("Patient not found.", 'error')
    
    return redirect(url_for('index'))

@app.route('/search_result', methods=['POST'])
def search_result():
    search_criteria = request.form['search_criteria']
    found_patients = []
    for patient in patients.values():
        if (
            search_criteria.lower() in patient.name.lower() or
            search_criteria == patient.id or
            search_criteria == patient.mobile_no
        ):
            found_patients.append(patient)
    
    return render_template('search_result.html', found_patients=found_patients)

def generate_patient_id():
    return str(random.randint(1000000000, 9999999999))

if __name__ == '__main__':
    app.run(debug=True)
